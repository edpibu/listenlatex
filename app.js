const port = 8080;

const { spawn } = require('child_process');
const path = require('path');
const fs = require('fs');
const debug = require('debug')('listenlatex:server');
const debuglatex = require('debug')('listenlatex:latex');
const express = require('express');
const fileUpload = require('express-fileupload');

const app = express();
app.use(fileUpload());


app.route('/')
    .post((req, res) => {
        debug(`Working on ${req.files.main.name}...`)

        for(let name in req.files) {
            fs.writeFileSync(
                path.join('tmp', req.files[name].name),
                req.files[name].data,
            );
        }

        let tex = spawn('xelatex', [
            '-interaction=nonstopmode',
            req.files.main.name,
        ], {cwd: 'tmp'});

        tex.stdout.on('data', data => debuglatex(`${data}\n`));

        tex.on('close', code => {
            let tex2 = spawn('xelatex', [
                '-interaction=nonstopmode',
                req.files.main.name,
            ], {cwd: 'tmp'});

            tex2.stdout.on('data', data => debuglatex(`${data}\n`));
            tex2.on('close', code => {
                res.sendFile(path.format({
                    dir: path.join(process.cwd(), 'tmp'),
                    name: path.parse(req.files.main.name).name,
                    ext: '.pdf',
                }));
                debug(`Finished working on ${req.files.main.name}!`)
            });
        });
    });

app.listen(port,
    () => debug(`ListenLatex listening on port ${port}...`));
